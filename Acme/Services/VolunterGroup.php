<?php
namespace Acme\Services;

use Acme\Repositories\VolunteerGroup as Repository;

use Acme\Common\DataFields\VolunteerGroup as DataField;
use Acme\Common\Entity\VolunteerGroup as Entity;

use Acme\Common\Constants as Constants;

class VolunteerGroup extends Services{

    protected $repository;
    
    public function __construct()
	{
		$this->repository = new Repository;
	}

    public function getAll(){
        $result = $this->repository->getAll();

        return $result;
    }

    public function getByID($id){
        $result = $this->repository->getByID($id);

        return $result;
    }

    public function list($request){
        $result = $this->repository->list($request);

        return $result;
    }

    public function show($id){

        $result = $this->repository->show($id);

        return $result;
    }

    public function destroy($id){

        $result = $this->repository->destroy($id);

        return $result;
    }

    public function create($entity){

       $result = $this->repository->create($entity);

       return $result;
    }

    public function update($entity , $id){
       
       $result = $this->repository->update($entity , $id);

       return $result;
    }

    public function save($entity){
        
        $result = $this->repository->save($entity);

        return $result;
    }

    public function delete($id)
    {
        $result = $this->repository->delete($id);

        return $result;
    }

    public function getAllActive(){
        $result = $result = $this->repository->getAllActive();

        return $result;
    }

    public function keyValuePair(){
        $result = $this->repository->getAll()->map(function ($item){
            return collect([
                "key" => $item[DataField::ID],
                "value" => $item[DataField::VALUE]
            ]);
        });

        return $result;
    }
}