<?php
namespace Acme\Common\Entity;
use Acme\Common\DataFields\VolunteerGroup as DataField;

class VolunteerGroup
{
    public $VolunteerGroupID = "";
    public $UserID = "";
    public $CompanyName = "";
    public $Person = "";
    public $Contact = "";
    public $Address = "";
    public $Type = "";
    public $Status = "";

    public function Validate()
    {
       
    }
    
    public function SetData($input)
    {
        $this->VolunteerGroupID = !isset($input["id"])? 0:$input["id"]; 
        $this->UserID = !isset($input["user_id"])? 0:$input["user_id"];
        $this->CompanyName = !isset($input["company_name"])? "":$input["company_name"];
        $this->Person = !isset($input["person"])? "":$input["person"];
        $this->Contact = !isset($input["contact"])? "":$input["contact"];
        $this->Address = !isset($input["address"])? "":$input["address"];
        $this->Type = !isset($input["type"])? 0:$input["type"];
        $this->Status = !isset($input["status"])? 1:$input["status"];
    }

    public function Serialize()
    {
        $data = array();

        $data[Datafield::ID] = $this->VolunteerGroupID;
        $data[Datafield::USER_ID] = $this->UserID;
        $data[Datafield::COMPANY_NAME] = $this->CompanyName;
        $data[Datafield::PERSON] = $this->Person;
        $data[Datafield::CONTACT] = $this->Contact;
        $data[Datafield::ADDRESS] = $this->Address;
        $data[Datafield::TYPE] = $this->Type;
        $data[Datafield::STATUS] = $this->Status;

        return $data;
    }

    public function Deserialize($data)
    {
        $result = array();

        $result['id'] = $data[Datafield::ID];
        $result['user_id'] = $data[Datafield::USER_ID];
        $result['company_name'] = $data[Datafield::COMPANY_NAME];
        $result['person'] = $data[Datafield::PERSON];
        $result['contact'] = $data[Datafield::CONTACT];
        $result['address'] = $data[Datafield::ADDRESS];
        $result['type'] = $data[Datafield::TYPE];
        $result['status'] = $data[Datafield::STATUS];

        return $result;
    }

}


?>