<?php
namespace Acme\Common\Entity;
use Acme\Common\DataFields\Volunteer as DataField;

class Volunteer
{
    public $VolunteerID = "";
    public $VolunteerGroupID = "";
    public $Lastname = "";
    public $Firstname = "";
    public $Middlename = "";
    public $Gender = "";
    public $Birthday = "";
    public $Address = "";
    public $Contact = "";
    public $Rank = "";
    public $Status = 1;

    public function Validate()
    {
       
    }
    
    public function SetData($input)
    {
        $this->VolunteerID = !isset($input["id"])? 0:$input["id"]; 
        $this->VolunteerGroupID = !isset($input["volunteer_group_id"])? 0:$input["volunteer_group_id"];
        $this->Lastname = !isset($input["lastname"])? "":$input["lastname"];
        $this->Firstname = !isset($input["firstname"])? "":$input["firstname"];
        $this->Middlename = !isset($input["middlename"])? "":$input["middlename"];
        $this->Gender = !isset($input["gender"])? "":$input["gender"];
        $this->Birthday = !isset($input["birthday"])? "":$input["birthday"];
        $this->Address = !isset($input["address"])? "":$input["address"];
        $this->Contact = !isset($input["contact"])? "":$input["contact"];
        $this->Rank = !isset($input["rank"])? "":$input["rank"];
  
    }

    public function Serialize()
    {
        $data = array();

    
        $data[Datafield::ID] = $this->VolunteerID;
        $data[Datafield::VOLUNTEER_GROUP_ID] = $this->VolunteerGroupID;
        $data[Datafield::LASTNAME] = $this->Lastname;
        $data[Datafield::FIRSTNAME] = $this->Firstname;
        $data[Datafield::MIDDLENAME] = $this->Middlename;
        $data[Datafield::GENDER] = $this->Gender;
        $data[Datafield::BIRTHDAY] = $this->Birthday;
        $data[Datafield::ADDRESS] = $this->Address;
        $data[Datafield::CONTACT] = $this->Contact;
        $data[Datafield::RANK] = $this->Rank;
        $data[Datafield::STATUS] = $this->Status;

        return $data;
    }

    public function Deserialize($data)
    {
        $result = array();

        $result['id'] = $data[Datafield::ID];
        $result['volunteer_group_id'] = $data[Datafield::VOLUNTEER_GROUP_ID];
        $result['lastname'] = $data[Datafield::LASTNAME];
        $result['firstname'] = $data[Datafield::FIRSTNAME];
        $result['middlename'] = $data[Datafield::MIDDLENAME];
        $result['gender'] = $data[Datafield::GENDER];
        $result['birthday'] = $data[Datafield::BIRTHDAY];
        $result['address'] = $data[Datafield::ADDRESS];
        $result['contact'] = $data[Datafield::CONTACT];
        $result['rank'] = $data[Datafield::RANK];
        $result['status'] = $data[Datafield::STATUS];

        return $result;
    }

}


?>