<?php
namespace Acme\Common\Entity;
use Acme\Common\DataFields\Service as DataField;

class Service
{
    public $ServiceID = "";
    public $Name = "";
    public $Description = "";
    public $Status = 1;
    public $IsDeleted = 0;

    public function Validate()
    {
       
    }
    
    public function SetData($input)
    {
        $this->ServiceID = !isset($input["id"])? 0:$input["id"]; 
        $this->Name = !isset($input["name"])? 0:$input["name"];
        $this->Description = !isset($input["description"])? "":$input["description"];
        $this->Status = !isset($input["status"])? "":$input["status"];
        //$this->IsDeleted = !isset($input["is_deleted"])? "":$input["is_deleted"];

    }

    public function Serialize()
    {
        $data = array();

        $data[Datafield::ID] = $this->ServiceID;
        $data[Datafield::NAME] = $this->Name;
        $data[Datafield::DESCRIPTION] = $this->Description;
        $data[Datafield::STATUS] = $this->Status;
        $data[Datafield::IS_DELETED] = $this->IsDeleted;

        return $data;
    }

    public function Deserialize($data)
    {
        $result = array();

        $result['id'] = $data[Datafield::ID];
        $result['name'] = $data[Datafield::NAME];
        $result['description'] = $data[Datafield::DESCRIPTION];
        $result['status'] = $data[Datafield::STATUS];
        $result['is_deleted'] = $data[Datafield::IS_DELETED];

        return $result;
    }

}


?>