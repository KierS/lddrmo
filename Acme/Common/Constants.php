<?php
namespace Acme\Common;

class Constants
{
    #GENERAL
    const LIMIT = 20;
    const ALL = 'All';
    const SYMBOL_ALL = '*';
    const JSON = "json";
    const ID = "id";
    const ADJUST_AMOUNT = "adjust-amount";
    const SECRET = 'secret';
    const EMPTY = '';
    const ACTIVE = 1;
    const FIRST_INDEX = 0;
    const LINE_BREAK = '<br>';
    const PASSWORD_CONFIRMATION = 'password_confirmation';

    const _TRUE = 1;
    const _FALSE = 0;

    #GENERAL ERROR
    const ERROR_AUTHENTICATION = "Authentication Expired.";
    const LOST_CONNECTION = "Connection Failed.";

    #Pagination
    const PAGE_INDEX = "page";
    const PAGE_SIZE = "PageSize";
    const KEYWORD = "Search";
    const SORT_ORDER = "SortOrder";
    const SORT_BY = "SortBY";


    #FORMAT
    const INPUT_DATE_FORMAT     = 'Y-m-d';
    const OUTPUT_DATE_FORMAT    = 'F d,Y';
    const ROW_DATE_TIME_FORMAT = 'Y-m-d H:i:s';
    const LIST_DATE_FORMAT = "m/d/Y";
    const LIST_DATE_TIME_FORMAT = "m/d/Y H:i:s";
    const ANDROID_DATETIME_FORMAT = 'yyyy-MM-dd hh:mm:ss';
    const ANDROID_DATE_FORMAT = 'yyyy-MM-dd';

    #DEFAULT VALUES
    const DEFAULT_SORT_ORDER = "DESC" ;
    const DEFAULT_SORT_BY = "id";

    const DESC = "DESC";
    const ASC = "ASC";

    #LOGIN
    const LOGIN_SUCCESS = "Successfully Logged-In";
    const LOGIN_FAILED = "Invalid Username and Password";
    const SUCCESSFULLY_REGISTERED = 'Successfully registered. You are now logged in';

    #VIEWS PAGES
    const ERROR_PAGE = "errors.errorpage";

    #ERROR_CODE
    const ERROR_CODE = "error_code";
    const ERROR_AUTHENTICATION_EXPIRED = 401;
    const ERROR_OUTDATED = 402;
    const ERROR_INACTIVE = 403;

    const ERROR_OUTDATED_APPLICATION = "The app you are using is outdated, Please contact organization administrator";
    const ERROR_INACTIVE_APPLICATION = "The app you are using was deactivated, Please contact organization administrator";
    #TIME
    const ONE_DAY = 86399;



   

}

?>