<?php
namespace Acme\Common\DataFields;

class Service
{
    const TABLE_NAME = "services";

    const ID = "ServiceID";
    const NAME = "Name";
    const DESCRIPTION = "Description";
    const STATUS = "Status";
    const IS_DELETED = "IsDeleted";
    
}


?>