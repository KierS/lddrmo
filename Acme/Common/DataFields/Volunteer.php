<?php
namespace Acme\Common\DataFields;

class Volunteer
{
    const TABLE_NAME = "volunteers";

    const ID = "VolunteerID";
    const VOLUNTEER_GROUP_ID = "VolunteerGroupID";
    const LASTNAME = "Lastname";
    const FIRSTNAME = "Firstname";
    const MIDDLENAME = "Middlename";
    const GENDER = "Gender";
    const BIRTHDAY = "Birthday";
    const ADDRESS = "Address";
    const CONTACT = "Contact";
    const RANK = "Rank";
    const STATUS = "Status";
    
}


?>