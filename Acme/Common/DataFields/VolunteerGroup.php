<?php
namespace Acme\Common\DataFields;

class VolunteerGroup
{
    const TABLE_NAME = "volunteer_groups";

    const ID = "VolunteerGroupID";
    const USER_ID = "UserID";
    const COMPANY_NAME = "CompanyName";
    const PERSON = "Person";
    const CONTACT = "Contact";
    const ADDRESS = "Address";
    const TYPE = "Type";
    const STATUS = "Status";
    const IS_DELETED = "IsDeleted";
    
    const VALUE = "CompanyName";
}


?>