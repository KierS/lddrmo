<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVolunteersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteers', function (Blueprint $table) {
            $table->bigIncrements('VolunteerID');
            $table->integer('VolunteerGroupID');
            $table->string('Lastname');
            $table->string('Firstname');
            $table->string('Middlename');
            $table->string('Gender');
            $table->string('Birthday');
            $table->string('Address');
            $table->string('Contact');
            $table->integer('Rank');
            $table->integer('Status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volunteers');
    }
}
