<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVolunteerGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteer_groups', function (Blueprint $table) {
            $table->bigIncrements('VolunteerGroupID');
            $table->integer('UserID');
            $table->string('CompanyName');
            $table->string('Person');
            $table->string('Contact');
            $table->string('Address');
            $table->string('Type');
            $table->integer('Status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volunteer_groups');
    }
}
