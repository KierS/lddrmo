<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VolunteerGroups extends Model
{
    //
     protected $table = 'volunteer_groups';

     protected $guarded = ['VolunteerGroupID'];
}
