<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;

use Acme\Services\Volunteer as Services;
use Acme\Services\VolunteerGroup as VolunteerGroupServices;

use Acme\Common\CommonFunction;
use Acme\Common\DataResult as DataResult;
use Acme\Common\DataFields\Volunteer as DataField;
use Acme\Common\Entity\Volunteer as Entity;
use Acme\Common\Constants as Constants;

class VolunteerController extends Controller
{
    use CommonFunction;

    protected $services;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        
        $this->services = new Services;
        $this->volunteer_group = new VolunteerGroupServices;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $options["volunteer_groups"] = $this->volunteer_group->keyValuePair();
        return view('admin.volunteers.list')->with([
            'options' => $options
        ]);;
    }

    public function create(Request $request)
    {
        $result = new DataResult;
        //
        try{
            $input = $request->all();

            $entity = new Entity;
            $entity->SetData($input);
            $data = $entity->Serialize();

            $result->data = $this->services->create($data);
            $result->message = 'Success';
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $result = new DataResult;

        try{
            $input = $request->all();

            $entity = new Entity;
            
            $entity->SetData($input);
            $data = $entity->Serialize();

            $result->data = $this->services->save($data);
            $result->data = $data;
            $result->message = 'Success';

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = new DataResult;
        try{
            $entity = new Entity;

            $data = $this->services->getByID($id);
            $data = $entity->Deserialize($data);

            $result->message = 'Success';
            $result->data = $data;
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result = new DataResult;
        //
        try{
            $input = $request->all();

            $entity = new Entity;
            $entity->SetData($input);
            $data = $entity->Serialize();

            $result->data = $this->services->update($data ,$id);
            $result->message = 'Success';
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $result = new DataResult;

        try{
            $result->data = $this->services->destroy($id);
            $result->message = 'Success';
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }
        return response()->json($result, 200);
    }

    public function delete(Request $request)
    {
        $result = new DataResult;

        try{

            $input = $request->all();
            $result->data = $this->services->delete($input[Constants::ID]);
            $result->message = 'Success';

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }


    public function list(Request $request)
    {
        $result = new DataResult;

        try{

            $input = $request->all();
            $result->data = $this->services->getAll();
            $result->message = 'Success';

        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }

    public function test()
    {
        $result = new DataResult;

        try{
            return $this->volunteer_group->keyValuePair();
        }catch(Exception $e) {
            $result = $this->RequestError($e);
        }

        return response()->json($result, 200);
    }
}
