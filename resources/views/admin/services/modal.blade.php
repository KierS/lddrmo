<div class="modal in" id="module-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="adjustModalTitle">Service Form</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="tips-message"></div>
            {!! Form::open([ 'class' => '"form-inline']) !!}
                     <div class="form-group bmd-form-group full-width">
                        <label for="name" class="bmd-label-floating left-indent">Name</label>
                        {{ Form::text('name', null, ['id' => 'name', 'label' => 'Name', 'class' => 'form-control field vrequired']) }}
                    </div>
                    <div class="form-group bmd-form-group full-width">
                        <label for="sim" class="bmd-label-floating left-indent">Description</label>
                        {{ Form::text('description', null, ['id' => 'description', 'label' => 'Description', 'class' => 'form-control field vrequired']) }}
                    </div>
                {{ Form::hidden('id', '' , ['id' => 'id', 'class' => 'field']) }}
            {!! Form::close() !!}
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save-btn">Save changes</button>
        </div>
    </div>
    </div>
</div>