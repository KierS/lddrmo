@extends('layouts.theme')

@section('header')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="index.html">Volunteer Groups</a>
          </li>
        </ol>
@endsection

@section('content')
<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Volunter Groups
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" style="float:right" onClick="AddModuleForm();">
                <span class="btn-label">Add</span>
            </button>
          </div>
            
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Person</th>
                    <th>Contact</th>
                    <th>Address</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Person</th>
                    <th>Contact</th>
                    <th>Address</th>
                    <th>Actions</th>
                  </tr>
                </tfoot>
                <tbody id="list-result">
                  
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
@endsection


@section('modals')
  @include('admin.volunteer_groups.modal')
@endsection

@section('scripts')
    <script src="{{ asset('js/volunteer_group.js') }}"></script>
    <script src="{{ asset('js/volunteer_group.ui.js') }}"></script>
@endsection

@section('jquery-tmpl')
  <script type="text/x-jQuery-tmpl" id="list-tmpl">
    <tr>
      <td>${ CompanyName}</td>
      <td>${ Person }</td>
      <td>${ Contact}</td>
      <td>${ Address }</td>
      <td>
        <button type="button" class="btn btn-sm btn-primary" onClick="Initialize('${ VolunteerGroupID }')">Edit</button>
        <button type="button" class="btn btn-sm btn-danger" onClick="Remove('${ VolunteerGroupID }')">Delete</button>
      </td>
    </tr>
  </script>
@endsection