<div class="modal in" id="module-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="adjustModalTitle">Volunteer Group Form</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="tips-message"></div>
            {!! Form::open([ 'class' => '"form-inline']) !!}
                     <div class="form-group bmd-form-group full-width">
                        <label for="registerTo" class="bmd-label-floating left-indent">Company Name</label>
                        {{ Form::text('companyName', null, ['id' => 'company_name', 'label' => 'Company Name', 'class' => 'form-control field vrequired']) }}
                    </div>
                    <div class="form-group bmd-form-group full-width">
                        <label for="sim" class="bmd-label-floating left-indent">Person</label>
                        {{ Form::text('person', null, ['id' => 'person', 'label' => 'Person', 'class' => 'form-control field vrequired']) }}
                    </div>
                    <div class="form-group bmd-form-group full-width">
                        <label for="sim" class="bmd-label-floating left-indent">Contact</label>
                        {{ Form::text('contact', null, ['id' => 'contact', 'label' => 'Contact', 'class' => 'form-control field vrequired']) }}
                    </div>
                    <div class="form-group bmd-form-group full-width">
                        <label for="template" class="bmd-label-floating left-indent">Address</label>
                        {{ Form::text('address', null, ['id' => 'address', 'label' => 'Address', 'class' => 'form-control field']) }}
                    </div>
                    <div class="form-group bmd-form-group full-width">
                        <label for="balance" class="bmd-label-floating left-indent">Type</label>
                        {{ Form::number('type', null, ['id' => 'type', 'label' => 'Type', 'class' => 'form-control field vrequired']) }}
                    </div>
                {{ Form::hidden('id', '' , ['id' => 'id', 'class' => 'field']) }}
            {!! Form::close() !!}
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save-btn">Save changes</button>
        </div>
    </div>
    </div>
</div>