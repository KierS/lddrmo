@extends('layouts.theme')

@section('header')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="index.html">Volunteers</a>
          </li>
        </ol>
@endsection

@section('content')
<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Volunter
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" style="float:right" onClick="AddModuleForm();">
                <span class="btn-label">Add</span>
            </button>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Lastname</th>
                    <th>Firstname</th>
                    <th>Contact</th>
                    <th>Rank</th>
                    <th>Availbility</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Lastname</th>
                    <th>Firstname</th>
                    <th>Contact</th>
                    <th>Rank</th>
                    <th>Availbility</th>
                    <th>Actions</th>
                  </tr>
                </tfoot>
                <tbody id="list-result">
                  
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
@endsection

@section('modals')
  @include('admin.volunteers.modal')
@endsection

@section('scripts')
    <script src="{{ asset('js/volunteer.js') }}"></script>
    <script src="{{ asset('js/volunteer.ui.js') }}"></script>
@endsection

@section('jquery-tmpl')
  <script type="text/x-jQuery-tmpl" id="list-tmpl">
    <tr>
      <td>${ Lastname }</td>
      <td>${ Firstname }</td>
      <td>${ Contact }</td>
      <td>${ Rank }</td>
      <td>OK</td>
      <td>
        <button type="button" class="btn btn-sm btn-primary" onClick="Initialize('${ VolunteerID }')">Edit</button>
        <button type="button" class="btn btn-sm btn-danger" onClick="Remove('${ VolunteerID }')">Delete</button>
      </td>
    </tr>
  </script>
@endsection