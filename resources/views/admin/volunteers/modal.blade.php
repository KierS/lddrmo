<div class="modal in" id="module-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="adjustModalTitle">Volunteer Form</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="tips-message"></div>
            {!! Form::open([ 'class' => '"form-inline']) !!}
                    <div class="container">
                        <div class="row">
                            <div class="col-sm">
                                <div class="form-group bmd-form-group full-width">
                                    <label for="volunteer_group_id" class="bmd-label-floating left-indent">Volunteer Group</label>
                                    <select name="volunteer_group_id" id="volunteer_group_id" class="form-control field vrequired">
                                     <option value="" selected disabled>-- Select --</option>
                                     @foreach ($options['volunteer_groups'] as $list)
                                        <option value="{{ $list["key"] }}">{{ $list["value"] }}</option>
                                     @endforeach
                                     </select>
                                </div>
                                <div class="form-group bmd-form-group full-width">
                                    <label for="registerTo" class="bmd-label-floating left-indent">Lastname</label>
                                    {{ Form::text('lastname', null, ['id' => 'lastname', 'label' => 'Lastname', 'class' => 'form-control field vrequired']) }}
                                </div>
                                <div class="form-group bmd-form-group full-width">
                                    <label for="sim" class="bmd-label-floating left-indent">Firstname</label>
                                    {{ Form::text('firstname', null, ['id' => 'firstname', 'label' => 'Firstname', 'class' => 'form-control field vrequired']) }}
                                </div>
                                <div class="form-group bmd-form-group full-width">
                                    <label for="sim" class="bmd-label-floating left-indent">Middlename</label>
                                    {{ Form::text('middlename', null, ['id' => 'middlename', 'label' => 'Middlename', 'class' => 'form-control field vrequired']) }}
                                </div>
                                <div class="form-group bmd-form-group full-width">
                                    <label for="template" class="bmd-label-floating left-indent">Gender</label>
                                    {{-- Form::text('gender', null, ['id' => 'gender', 'label' => 'Gender', 'class' => 'form-control field']) --}}
                                    <select name="gender" id="gender" class="form-control field">
                                        <option value="" selected disabled>-- Select --</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm">
                                <div class="form-group bmd-form-group full-width">
                                    <label for="template" class="bmd-label-floating left-indent">Birthday</label>
                                    {{ Form::date('birthday', null, ['id' => 'birthday', 'label' => 'Birthday', 'class' => 'form-control field']) }}
                                </div>
                                <div class="form-group bmd-form-group full-width">
                                    <label for="template" class="bmd-label-floating left-indent">Address</label>
                                    {{ Form::text('address', null, ['id' => 'address', 'label' => 'Address', 'class' => 'form-control field']) }}
                                </div>
                                <div class="form-group bmd-form-group full-width">
                                    <label for="balance" class="bmd-label-floating left-indent">Contact</label>
                                    {{ Form::text('contact', null, ['id' => 'contact', 'label' => 'Type', 'class' => 'form-control field vrequired']) }}
                                </div>
                                <div class="form-group bmd-form-group full-width">
                                    <label for="balance" class="bmd-label-floating left-indent">Rank</label>
                                    {{ Form::text('rank', null, ['id' => 'rank', 'label' => 'Rank', 'class' => 'form-control field vrequired']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                {{ Form::hidden('id', '' , ['id' => 'id', 'class' => 'field']) }}
            {!! Form::close() !!}
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save-btn">Save changes</button>
        </div>
    </div>
    </div>
</div>