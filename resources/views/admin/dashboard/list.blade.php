@extends('layouts.theme')

@section('header')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="index.html">Monitoring Board</a>
          </li>
        </ol>
@endsection

@section('content')
  <div>
      <table>
        <tr>
        <td>
          <div class="card" id="info-container" style="width: 18rem;">
            <img class="card-img-top" src="{{ asset('images/no-image.png') }}" alt="Card image cap" id="info-image">
            <div class="card-body">
              <h5 class="card-title">EMR Request</h5>
              <table>
                  <tr>
                      <td id="info-label">Caller's Name:</td>
                      <td id="info-text">Alvarez, Gia</td>
                  </tr>
                  <tr>
                      <td id="info-label">Contact Number:</td>
                      <td id="info-text">09276972622</td>
                  </tr>
                  <tr>
                      <td id="info-label">Address:</td>
                      <td id="info-text">Brgy Bata, Bacolod City</td>
                  </tr>
              </table>
              <a href="#" class="btn btn-primary" onClick="ViewForm()">View</a>
            </div>
          </div>
        </td>

        <td>
        <div class="card" id="info-container" style="width: 18rem;">
          <img class="card-img-top" src="{{ asset('images/no-image.png') }}" alt="Card image cap" id="info-image">
          <div class="card-body">
            <h5 class="card-title">Fire Report</h5>
            <table>
                <tr>
                    <td id="info-label">Caller's Name:</td>
                    <td id="info-text">Homer, Kate</td>
                </tr>
                <tr>
                    <td id="info-label">Contact Number:</td>
                    <td id="info-text">09196972651</td>
                </tr>
                <tr>
                    <td id="info-label">Address:</td>
                    <td id="info-text">Brgy Bata, Bacolod City</td>
                </tr>
            </table>
            <a href="#" class="btn btn-primary" onClick="ViewForm()">View</a>
          </div>
        </div>
        </td>
      </tr>
      </table>
  </div>
@endsection

@section('modals')
  @include('admin.dashboard.report-modal')
@endsection

@section('scripts')
    <script src="{{ asset('js/dashboard.ui.js') }}"></script>
@endsection