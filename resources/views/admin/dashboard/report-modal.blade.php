<div class="modal in" id="module-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog" style="margin-left:20px"role="document">
    <div class="modal-content" style="width:1300px;height:600px">
        <div class="modal-header">
            <h5 class="modal-title" id="adjustModalTitle">Request</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div>
                <table>
                    <tr>
                        <td>
                            <div style="border:1px solid #ccc;width:350px;height:300px;">
                                <img src="{{ asset('images/map.png') }}" style="width:99%;height:99%"/>
                            </div>
                        </td>
                        <td>
                            <div style="border:1px solid #ccc;width:400px;height:300px;">
                                <table style="width:300px;">
                                        <tr>
                                            <td colspan="2"><h5 class="card-title">Fire Report</h5></td>
                                        <tr>
                                        <tr>
                                            <td id="info-label">Caller's Name:</td>
                                            <td id="info-text">Alvarez, Gia</td>
                                        </tr>
                                        <tr>
                                            <td id="info-label">Contact Number:</td>
                                            <td id="info-text">09276972622</td>
                                        </tr>
                                        <tr>
                                            <td id="info-label">Address:</td>
                                            <td id="info-text">Brgy Bata, Bacolod City</td>
                                        </tr>
                                </table>
                            </div>
                        </td>
                        <td>
                            <div style="border:1px solid #ccc;width:500px;height:300px;">
                                <table border=1 >
                                    <tr>
                                        <td>Alert Level</td>
                                        <td>
                                            <select>
                                                <option value=""> 1 </option>
                                                <option value=""> 2 </option>
                                                <option value=""> 3 </option>
                                                <option value=""> 4 </option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Fire truck</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Ambulance</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Police</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save-btn">Save changes</button>
        </div>
    </div>
    </div>
</div>