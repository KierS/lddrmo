@extends('layouts.theme')

@section('header')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="index.html">Services</a>
          </li>
        </ol>
@endsection

@section('content')
<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Users</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Lastname</th>
                    <th>Firstname</th>
                    <th>Address</th>
                    <th>Birthday</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Lastname</th>
                    <th>Firstname</th>
                    <th>Address</th>
                    <th>Birthday</th>
                    <th>Actions</th>
                  </tr>
                </tfoot>
                <tbody>
                  <tr>
                    <td>Cheng</td>
                    <td>Jonathan</td>
                    <td>Brgy. Mandalagan, Bacolod City</td>
                    <td>July 21, 1998</td>
                    <td>
                      <button type="button" class="btn btn-sm btn-primary">Edit</button>
                      <button type="button" class="btn btn-sm btn-danger">Delete</button>
                    </td>
                  </tr>
                  <tr>
                    <td>Saudia</td>
                    <td>Mae</td>
                    <td>Brgy. Alijis, Bacolod City</td>
                    <td>April 04, 1994</td>
                    <td>
                      <button type="button" class="btn btn-sm btn-primary">Edit</button>
                      <button type="button" class="btn btn-sm btn-danger">Delete</button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
@endsection