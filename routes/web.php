<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index')->name('home');

Auth::routes();

Route::get('/home', 'DashboardController@index')->name('home');

Route::group([
    'prefix' => 'volunteergroups',
    'middleware' => 'auth',
], function(){
    Route::get('/', ['as' => 'admin.volunteergroups', 'uses' => 'VolunteerGroupController@index']);
    Route::post('list', ['as' => 'admin.volunteergroups.list', 'uses' => 'VolunteerGroupController@list']);
    Route::get('initialize/{id}', ['as' => 'admin.volunteergroups.edit', 'uses' => 'VolunteerGroupController@edit']);
    Route::post('save', ['as' => 'admin.volunteergroups.save', 'uses' => 'VolunteerGroupController@store']);
    Route::post('delete', ['as' => 'admin.volunteergroups.delete', 'uses' => 'VolunteerGroupController@delete']);
});

Route::group([
    'prefix' => 'volunteers',
    'middleware' => 'auth',
], function(){
    Route::get('/', ['as' => 'admin.volunteers', 'uses' => 'VolunteerController@index']);
    Route::post('list', ['as' => 'admin.volunteers.list', 'uses' => 'VolunteerController@list']);
    Route::get('initialize/{id}', ['as' => 'admin.volunteers.edit', 'uses' => 'VolunteerController@edit']);
    Route::post('save', ['as' => 'admin.volunteers.save', 'uses' => 'VolunteerController@store']);
    Route::post('delete', ['as' => 'admin.volunteers.delete', 'uses' => 'VolunteerController@delete']);
});

Route::group([
    'prefix' => 'services',
    'middleware' => 'auth',
], function(){
    Route::get('/', ['as' => 'admin.services', 'uses' => 'ServiceController@index']);
     Route::post('list', ['as' => 'admin.service.list', 'uses' => 'ServiceController@list']);
    Route::get('initialize/{id}', ['as' => 'admin.service.edit', 'uses' => 'ServiceController@edit']);
    Route::post('save', ['as' => 'admin.service.save', 'uses' => 'ServiceController@store']);
    Route::post('delete', ['as' => 'admin.service.delete', 'uses' => 'ServiceController@delete']);
});

Route::group([
    'prefix' => 'users',
    'middleware' => 'auth',
], function(){
    Route::get('/', ['as' => 'admin.users', 'uses' => 'UserController@index']);
});


Route::get('/test', 'VolunteerController@test')->name('test');
