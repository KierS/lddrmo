var module;
$(document).ready(function()
{
	module  = new System.Service(); // <--- Initialize the module
	module.ListView();
    module.Load();
	
	$("#save-btn").click(function(){
		module.Save();
	});
	
	$("#clear-btn").click(function(){
		module.Clear();
	});

	$("#apply-filters-btn").click(function(){
		module.ListView();
	});

});

function ListView(url)
{
	module.ListView(url);
}

function Remove(id, name)
{
	module.Remove(id, name);
}

function Initialize(id)
{
	module.Clear();
	module.Initialize(id);
	module.SetReadOnly(false);
}

function AddModuleForm()
{
	module.Clear();
	openModal('module-modal');

}
